# Devires Framework Security

Classes para aplicar segurança JWT em microsseviços para reuso em projetos.

### Publicar no Repositório Maven Local

> ./gradlew publishToMavenLocal

### Declarar Repositório Local nos Projetos para Usar

```
repositories {
    ...
    mavenLocal()
}
```

### Publicar no Repositório Maven do GitLab Devires

> ./gradlew publish