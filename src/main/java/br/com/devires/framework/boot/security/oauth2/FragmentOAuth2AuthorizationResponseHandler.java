package br.com.devires.framework.boot.security.oauth2;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationCodeRequestAuthenticationToken;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

public class FragmentOAuth2AuthorizationResponseHandler implements AuthenticationSuccessHandler {

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        OAuth2AuthorizationCodeRequestAuthenticationToken authorizationCodeRequestAuthentication = (OAuth2AuthorizationCodeRequestAuthenticationToken) authentication;
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(authorizationCodeRequestAuthentication.getRedirectUri());
        String responseMode = request.getParameter("response_mode");
        if (StringUtils.equalsIgnoreCase(responseMode, "fragment")) {
            String fragment = OAuth2ParameterNames.CODE + "=" + authorizationCodeRequestAuthentication.getAuthorizationCode().getTokenValue();
            if (StringUtils.isNotBlank(authorizationCodeRequestAuthentication.getState())) {
                fragment += "&" + OAuth2ParameterNames.STATE + "=" + authorizationCodeRequestAuthentication.getState();
            }
            uriBuilder.fragment(fragment);
        } else {
            uriBuilder.queryParam(OAuth2ParameterNames.CODE, authorizationCodeRequestAuthentication.getAuthorizationCode().getTokenValue());
            if (StringUtils.isNotBlank(authorizationCodeRequestAuthentication.getState())) {
                uriBuilder.queryParam(OAuth2ParameterNames.STATE, authorizationCodeRequestAuthentication.getState());
            }
        }
        this.redirectStrategy.sendRedirect(request, response, uriBuilder.toUriString());
    }

}
