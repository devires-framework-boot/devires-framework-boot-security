package br.com.devires.framework.boot.security.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

/* Do NOT use @Component */
public class AuthorizationPassThroughRequestInterceptor implements RequestInterceptor {

    private static final String TOKEN_TYPE = "Bearer";

    @Override
    public void apply(RequestTemplate requestTemplate) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof Jwt) {
            Jwt jwt = (Jwt) authentication.getPrincipal();
            String accessToken = jwt.getTokenValue();
            requestTemplate.header(HttpHeaders.AUTHORIZATION, TOKEN_TYPE + " " + accessToken);
        }
    }

}
