package br.com.devires.framework.boot.security.handler;

import br.com.devires.framework.boot.commons.model.CommonErrors;
import br.com.devires.framework.boot.commons.model.ErrorModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ModelExceptionAccessDeniedHandler implements AccessDeniedHandler {

    private ObjectMapper objectMapper;

    public ModelExceptionAccessDeniedHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) throws IOException, ServletException {
        ErrorModel errorModel = CommonErrors.UNAUTHORIZED.get();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        response.setStatus(errorModel.getStatus());
        response.getWriter().write(objectMapper.writeValueAsString(errorModel));
        response.flushBuffer();
    }

}
