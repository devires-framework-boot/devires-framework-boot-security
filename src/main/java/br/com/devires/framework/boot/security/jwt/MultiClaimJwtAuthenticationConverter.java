package br.com.devires.framework.boot.security.jwt;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.DelegatingJwtGrantedAuthoritiesConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MultiClaimJwtAuthenticationConverter extends JwtAuthenticationConverter {

    public MultiClaimJwtAuthenticationConverter () {
        List<Converter<Jwt, Collection<GrantedAuthority>>> converters = buildConverters();
        super.setJwtGrantedAuthoritiesConverter(new DelegatingJwtGrantedAuthoritiesConverter(converters));
    }

    private List<Converter<Jwt, Collection<GrantedAuthority>>> buildConverters() {
        List<Converter<Jwt, Collection<GrantedAuthority>>> converters = new ArrayList<>();
        {
            JwtGrantedAuthoritiesConverter withScope = new JwtGrantedAuthoritiesConverter();
            withScope.setAuthorityPrefix("SCOPE_");
            // use default claim names scope and scp
            converters.add(withScope);
        }
        {
            JwtGrantedAuthoritiesConverter withRoles = new JwtGrantedAuthoritiesConverter();
            withRoles.setAuthoritiesClaimName("roles");
            withRoles.setAuthorityPrefix("ROLE_");
            converters.add(withRoles);
        }
        {
            JwtGrantedAuthoritiesConverter withPermissions = new JwtGrantedAuthoritiesConverter();
            withPermissions.setAuthoritiesClaimName("permissions");
            withPermissions.setAuthorityPrefix("PERM_");
            converters.add(withPermissions);
        }
        {
            JwtGrantedAuthoritiesConverter withGroups = new JwtGrantedAuthoritiesConverter();
            withGroups.setAuthoritiesClaimName("groups");
            withGroups.setAuthorityPrefix("GROUP_");
            converters.add(withGroups);
        }
        {
            JwtGrantedAuthoritiesConverter withAuthorities = new JwtGrantedAuthoritiesConverter();
            withAuthorities.setAuthoritiesClaimName("authorities");
            withAuthorities.setAuthorityPrefix("");
            converters.add(withAuthorities);
        }
        return converters;
    }

}
