package br.com.devires.framework.boot.security.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;

/* Do NOT use @Component */
public class ClientCredentialsRequestInterceptor implements RequestInterceptor {

    private static final String TOKEN_TYPE = "Bearer";

    @Lazy
    @Autowired
    private OAuth2AuthorizedClientManager authorizedClientManager;

    // id configured under spring.security.oauth2.client.registration.{id}
    private String clientRegistrationId;

    public ClientCredentialsRequestInterceptor(String clientRegistrationId) {
        this.clientRegistrationId = clientRegistrationId;
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String accessToken = getAccessToken();
        requestTemplate.header(HttpHeaders.AUTHORIZATION, TOKEN_TYPE + " " + accessToken);
    }

    private String getAccessToken() {
        OAuth2AuthorizeRequest authorizeRequest = OAuth2AuthorizeRequest.withClientRegistrationId(clientRegistrationId)
                .principal(getEndUserAuthenticationToAttach()).build();
        OAuth2AuthorizedClient authorizedClient = authorizedClientManager.authorize(authorizeRequest);
        return authorizedClient.getAccessToken().getTokenValue();
    }

    private Authentication getEndUserAuthenticationToAttach() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            authentication = new AbstractAuthenticationToken(null) {
                @Override
                public Object getCredentials() {
                    return "";
                }
                @Override
                public Object getPrincipal() {
                    return "anonymous";
                }
            };
        }
        return authentication;
    }

}
