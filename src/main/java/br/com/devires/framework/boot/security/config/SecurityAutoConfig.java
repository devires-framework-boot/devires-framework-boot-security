package br.com.devires.framework.boot.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SecurityAutoConfig {

    @Bean
    public WebSecurityCustomizer ignoreActuatorSecurityCustomizer() {
        return securityCustomizer -> securityCustomizer.ignoring().requestMatchers("/actuator/**");
    }

    @Bean
    public WebSecurityCustomizer ignoreOpenApiCustomizer() {
        return securityCustomizer -> securityCustomizer.ignoring().requestMatchers("/swagger-ui/**", "/swagger.yaml/**");
    }

    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults("");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}